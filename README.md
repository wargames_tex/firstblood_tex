# First Blood 
## The Guadalcanal Campaign 

[[_TOC_]]

This is my rework of the classic [Avalon Hill International Kriegspeil
Society][ahiks] wargame [_First Blood_][grognard].

## Homepage 

This game, and other games I made Print'n'Play versions of, can be
found at
[$\mathrm{\LaTeX}$-wargames](https://wargames_tex.gitlab.io/wargame_www). 

## About this work

The rework consists of 

- Rewriting the rules in a more pedagogical way.  The structure of the
  rules follows the turn sequence more less.  The original rules was
  structured around the _command_ structure.  That is, ground troops
  where dealt with in a number of sections, then naval, installations,
  activities, and air units.  That meant one would have to search
  through the rules to find all things relevant for the game at some
  particular point in the game play.  The new structure puts things
  where they are used.  Command specific rules are still clearly
  marked.
- The movement point costs where not very clear.  For example, would
  one need to spend 2 MP to move from a hex with jungle terrain on the
  hex-side being passed over in to a clear terrain.  The examples in
  the original rules suggested that this was _not_ the case.  
  
  Also, the ridge rules where obscure.  The rules clearly stated that
  moving up and down the ridge would cost 2 MPs _both_, but since
  there is no ridge on the hex moved into, it made little sense.  The
  current map adds ridges on the down-slope too, making the rules and
  map simpler to understand.  That is, we _always_ look at the
  hex-edge crossed over and into when calculating movement cost.
  
  Finally, the original rules had _explicit_ MP costs for specific
  movements, but did not give general guidelines.  In the current
  version, there is a _base cost_ of one MP, and hex-sides then add
  _additional_ cost.
- Some places on the original map, there where hexes with partial
  jungle in them, but the hex was _not_ marked as jungle.  Clearly,
  the intention was that moving into these hexes would cost more, but
  that they should not be considered jungle for combat purposes.  In
  the current version, the rules stipulates that hex is considered
  "Jungle" _if and only if_ there's jungle terrain in the _middle_ of
  the hex.  The Hex-side moved over and into is  _always_ what counts
  for movement.  This simplifies the rules and map somewhat. 
- The text, graphics, and so on are all completely new, with the
  exception of the historical account. 
- On the board, two new boxes are introduced to keep track of airstrip
  status and to hold air craft units for refuelling.  One could
  potentially need to stack 10 chits in a single map hex, which isn't
  very feasible.  The status and air units can therefore be put in the
  separate boxes on the board.  In fact, it makes the game play a
  little easier since the air units are closer to the other air units
  of the game. 
- In the original rules there where no graphics illustrating the more
  subtle points of the rules.  I have introduced some illustrations
  showing how to read the rules to help the reader.  There are 8
  illustrations and 9 tables in total, and a collection of charts on
  the back of the booklet.
- The page count comes to 17 pages of rules, and 20 in total in the
  booklet (5 sheets of paper).

## The files 

The distribution consists of the following files 

- [FirstBloodA4.pdf][] This is a single document that contains
  everything: The rules, charts, separate Order of Battle (OOB) charts
  for the IJ and US factions, counters, and the board (in two parts).
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.  The rules will be 20 pages in
  this case.
  
- [FirstBloodA4Booklet.pdf][] This document _only_ contains the rules.
  It is meant to be printed on a duplex A4 printer with long edge
  binding.  This will produce 5 sheets which should be folded down the
  middle of the long edge, and stabled to form an A5 booklet of the
  rules.
  
- [materialsA4.pdf][] is a collection or charts, OOB charts, counters,
  and half-maps.
  
- [splitboardA3.pdf][] holds the _entire_ board.  It is meant to be
  printed on A3 paper.  Print and glue on to a relatively thick piece
  of cardboard or the like.  If an A3 printer is not available, one
  can use the two half-maps in the file [materialsA4.pdf][]
  

If you only have access to US Letter (and possibly Tabloid) printer,
you can use the following files 

| *A4 Series*                 | *Letter Series*                  |
| ----------------------------|----------------------------------|
| [FirstBloodA4.pdf][]        | [FirstBloodLetter.pdf][]  	     |
| [FirstBloodA4Booklet.pdf][] | [FirstBloodLetterBooklet.pdf][]  |
| [materialsA4.pdf][]         | [materialsLetter.pdf][]		     |
| [splitboardA3.pdf][]        | [splitboardTabloid.pdf][]	     |

Note, the US letter files are scaled down to 95% relative to the A4
files.  This is to fit everything in on the same scale.  This should
not be a problem for most users as most of the world is using A4
rather than the rather obscure Letter format.

Download [artifacts.zip][] for all files.

## VASSAL module 

There is a VASSAL module generated from the above documents via
[`export.tex`](export.tex), the Python script `export.py` from the
[`wargame`](https://gitlab.com/wargames_tex/wargame_tex`) package, and
the local patching script [`patch.py`](patch.py).

- [FirstBlood.vmod][]

You can also find the module on [the VASSAL
web-page](https://vassalengine.org/wiki/Module:First_Blood:_The_Guadalcanal_Campaign).
This module contains the rules and a tutorial. 

![Screenshot](.imgs/vassal.png "VASSAL module")
![Screenshot](.imgs/vassal-declare.png "VASSAL module")
![Screenshot](.imgs/vassal-resolve.png "VASSAL module")

## "Old-school" version 

An "Old-school" version, which emulates the original graphics from
AHIKS, is also available.

| *A4 Series*                   | *Letter Series*                    |
| ------------------------------|------------------------------------|
| [FirstBloodA4OS.pdf][]        | [FirstBloodLetterOS.pdf][]  	     |
| [FirstBloodA4BookletOS.pdf][] | [FirstBloodLetterBookletOS.pdf][]  |
| [materialsA4OS.pdf][]         | [materialsLetterOS.pdf][]		     |
| [splitboardA3OS.pdf][]        | [splitboardTabloidOS.pdf][]	     |

The VASSAL module is also available in this version: [FirstBloodOS.vmod][].

**TODO**: Make a tutorial

## Previews 

![Photo](.imgs/photo_all.jpg "Game components")
![Photo](.imgs/photo_oob.jpg "Close up of OOB")
![Photo](.imgs/photo_setup.jpg "Example setup")

![](.imgs/front.png)
![](.imgs/hexes.png)
![](.imgs/counters.png)
![](.imgs/us-oob.png)
![](.imgs/ij-oob.png)

**Ol'School version**

![](.imgs/os-front.png)
![](.imgs/os-hexes.png)
![](.imgs/os-counters.png)
![](.imgs/os-us-oob.png)
![](.imgs/os-ij-oob.png)

## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This
package, combined with the power of LaTeX, produces high-quality,
documents, with vector graphics to ensure that every thing scales.

## Original 

The original game was published in the [Avalon Hill
International Kriegspiel Society][ahiks] newsletter _Kommandeur_ Vol. 26, no
6 (1991).  It is made available for [free download][grognard].  Scans
of the original pages and graphics can also be found in the game's
[BoardGameGeek forum](https://drive.google.com/file/d/1SMNSHVqe7lgFyZ4_JNazYBmoj83GhgcD/view).

## Copyright and license 

This work is &#127279; 2022 Christian Holm Christensen, and licensed
under the [this license](LICENSE) (CC BY-SA 4.0).

## Alternative download links 

* A4:
  * [FirstBloodA4.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-A4-master/FirstBloodA4.pdf?job=dist)
  * [FirstBloodA4Booklet.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-A4-master/FirstBloodA4Booklet.pdf?job=dist)
  * [splitboardA3.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-A4-master/splitboardA3.pdf?job=dist)
  * [materialsA4.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-A4-master/materialsA4.pdf?job=dist)
* Letter:
  * [FirstBloodLetter.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-Letter-master/FirstBloodLetter.pdf?job=dist)
  * [FirstBloodLetterBooklet.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-Letter-master/FirstBloodLetterBooklet.pdf?job=dist)
  * [splitboardTabloid.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-Letter-master/splitboardTabloid.pdf?job=dist)
  * [materialsLetter.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-Letter-master/materialsLetter.pdf?job=dist)
* VASSAL module 
  * [FirstBlood.vmod](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-A4-master/FirstBlood.vmod?job=dist)
* Ol'school version 
  * A4
    * [FirstBloodA4OS.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-A4-master+OS/FirstBloodA4.pdf?job=dist)
    * [FirstBloodA4BookletOS.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-A4-master+OS/FirstBloodA4Booklet.pdf?job=dist)
    * [splitboardA3OS.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-A4-master+OS/splitboardA3.pdf?job=dist)
    * [materialsA4OS.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-A4-master+OS/materialsA4.pdf?job=dist)
  * Letter 
    * [FirstBloodLetterOS.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-Letter-master+OS/FirstBloodLetter.pdf?job=dist)
    * [FirstBloodLetterBookletOS.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-Letter-master+OS/FirstBloodLetterBooklet.pdf?job=dist)
    * [splitboardTabloidOS.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-Letter-master+OS/splitboardTabloid.pdf?job=dist)
    * [materialsLetterOS.pdf](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-Letter-master+OS/materialsLetter.pdf?job=dist)
  * VASSAL 
    * [FirstBloodOS.vmod](https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/raw/FirstBlood-A4-master+OS/FirstBlood.vmod?job=dist)

[ahiks]: https://ahiks.com
[grognard]: https://grognard.com/fb/
[artifacts.zip]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/download?job=dist

[FirstBloodA4.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-A4-master/FirstBloodA4.pdf?job=dist
[FirstBloodA4Booklet.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-A4-master/FirstBloodA4Booklet.pdf?job=dist
[splitboardA3.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-A4-master/splitboardA3.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-A4-master/materialsA4.pdf?job=dist

[FirstBloodLetter.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-Letter-master/FirstBloodLetter.pdf?job=dist
[FirstBloodLetterBooklet.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-Letter-master/FirstBloodLetterBooklet.pdf?job=dist
[splitboardTabloid.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-Letter-master/splitboardTabloid.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-Letter-master/materialsLetter.pdf?job=dist

[FirstBlood.vmod]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-A4-master/FirstBlood.vmod?job=dist

[FirstBloodA4OS.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-A4-master+OS/FirstBloodA4.pdf?job=dist
[FirstBloodA4BookletOS.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-A4-master+OS/FirstBloodA4Booklet.pdf?job=dist
[splitboardA3OS.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-A4-master+OS/splitboardA3.pdf?job=dist
[materialsA4OS.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-A4-master+OS/materialsA4.pdf?job=dist

[FirstBloodLetterOS.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-Letter-master+OS/FirstBloodLetter.pdf?job=dist
[FirstBloodLetterBookletOS.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-Letter-master+OS/FirstBloodLetterBooklet.pdf?job=dist
[splitboardTabloidOS.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-Letter-master+OS/splitboardTabloid.pdf?job=dist
[materialsLetterOS.pdf]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-Letter-master+OS/materialsLetter.pdf?job=dist

[FirstBloodOS.vmod]: https://gitlab.com/wargames_tex/firstblood_tex/-/jobs/artifacts/master/file/FirstBlood-A4-master+OS/FirstBlood.vmod?job=dist

#
#
#
NAME		:= FirstBlood
VERSION		:= 2.0
VMOD_VERSION	:= $(VERSION).0
VMOD_TITLE	:= First Blood: The Gaudalcanal Campaign
VMOD		:= $(NAME).vmod
TUTORIAL	:= Tutorial.vlog

DOCFILES	:= $(NAME).tex			\
		   counters.tex			\
		   history.tex			\
		   preface.tex			\
		   rules.tex			\
		   materials.tex
DATAFILES	:= tables.tex			\
		   oob.tex			\
		   counters.tex		
STYFILES	:= fb.sty			\
		   commonwg.sty			
BOARD		:= hexes.pdf
BOARDS		:= splitboardA4.pdf
LBOARDS		:= splitboardLetter.pdf
SIGNATURE	:= 20
PAGES		:= 27
PAPER		:= --a4paper
TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	\
		   splitboardA4.pdf 		\
		   splitboardA3.pdf 		\
		   materialsA4.pdf	

TARGETSLETTER	:= $(NAME)Letter.pdf 		\
		   $(NAME)LetterBooklet.pdf 	\
		   splitboardLetter.pdf 	\
		   splitboardTabloid.pdf 	\
		   materialsLetter.pdf

IMGS		:= .imgs

include Variables.mk
include Patterns.mk


$(IMGS)/%.png:%.pdf
	@echo "PDFTOCAIRO	Generating PNG $@"
	$(MUTE)pdftocairo $(IMG_FLAGS) -scale-to 600 -png -singlefile \
		$< $(basename $@) $(REDIR)
$(IMGS)/os-%.png:%.pdf
	@echo "PDFTOCAIRO	Generating PNG $@"
	$(MUTE)pdftocairo $(IMG_FLAGS) -scale-to 600 -png -singlefile \
		$< $(basename $@) $(REDIR)

all:	a4 letter vmod
a4:	$(TARGETSA4)
letter:	$(TARGETSLETTER)
vmod:	$(NAME).vmod
mine:	a4 vmod cover.pdf

oldschool:
	@echo "Creating 'old-school' versions'"
	$(MUTE)$(MAKE) clean
	$(MUTE)touch .oldschool
	$(MAKE) $(filter-out oldschool, $(MAKECMDGOALS))
	$(MUTE) rm -f .oldschool

labels%.tex:$(NAME)%.aux
	@echo "GREP		$@ ($<)"
	$(MUTE)grep "newlabel{sec:" $< > $@

materialsA4-%.pdf:materialsA4.pdf
	$(MUTE)pdfjam --outfile $@ -- $< '$*' $(REDIR)
	$(MUTE)pdfcrop $@ tmp.pdf 
	$(MUTE)mv tmp.pdf $@ 

charts.pdf:		materialsA4-1.pdf
	$(MUTE)mv $< $@

counters.pdf:		materialsA4-3.pdf
	$(MUTE)mv $< $@

us-oob.pdf:		materialsA4-5.pdf
	$(MUTE)mv $< $@

ij-oob.pdf:		materialsA4-7.pdf
	$(MUTE)mv $< $@


update-images:	$(IMGS)/front.png	\
		$(IMGS)/hexes.png 	\
		$(IMGS)/counters.png 	\
		$(IMGS)/charts.png	\
		$(IMGS)/ij-oob.png	\
		$(IMGS)/us-oob.png

update-images-os:
	$(MAKE) oldschool update-images IMGS:=$(IMGS)+OS
	for i in $(IMGS)+OS; do \
	  mv $$i $(IMGS)/os-basename $$i .png ; done

materialsA4.pdf:	materials.tex 	labelsA4.tex 		\
			$(BOARDS)	$(DATAFILES)	$(STYFILES)

$(NAME)A4.aux:		$(NAME).tex	$(BOARDS) 		\
			$(DOCFILES)	$(DATAFILES)	$(STYFILES)

materialsLetter.pdf:	materials.tex 	labelsLetter.tex	\
			$(LBOARDS)	$(DATAFILES) 	$(STYFILES)

$(NAME)Letter.aux:	$(NAME).tex	$(LBOARDS)	 	\
			$(DOCFILES) 	$(DATAFILES)	$(STYFILES)

export.pdf:		export.tex	labelsA4.tex		\
			$(DATAFILES)	$(STYFILES)

cover.pdf:		cover.tex	$(BOARDS)		\
			$(DATAFILES)	$(STYFILES)

include Rules.mk

clean::
	$(MUTE)rm -f .oldschool
	$(MUTE)rm -f splitboard*.tex
	$(MUTE)rm -f *.vtmp

realclean::
	$(MUTE)rm -f labels*.tex

include Docker.mk

distdirA4OS:
	@echo "DISTDIR	$(NAME)-A4-$(VERSION)+AHGC"
	$(MUTE)$(MAKE) oldschool a4 VMOD_TITLE:="$(VMOD_TITLE) - Ol'school"
	$(MUTE)$(MAKE) distdirA4 VERSION:=$(VERSION)+OS

distdirLetterOS:
	@echo "DISTDIR	$(NAME)-Letter-$(VERSION)+OS"
	$(MUTE)$(MAKE) oldschool letter VMOD_TITLE:="$(VMOD_TITLE) - Ol'school"
	$(MUTE)$(MAKE) distdirLetter VERSION:=$(VERSION)+OS

docker-artifacts:: distdirA4OS distdirLetterOS

.PRECIOUS:	$(NAME)Rules.pdf $(NAME).vtmp
#
# EOF
#


from wgexport import *
#
#

# --------------------------------------------------------------------
cbt_terrain = {
    ## 0
    '0000': 'jungle',
    '0100': 'jungle',
    '0200': 'jungle',
    '0300': '',
    '0400': '',
    '0500': '',
    '0600': '',
    '0700': 'beach',
    ## 1
    '0001': 'jungle',
    '0101': 'jungle',
    '0201': 'jungle',
    '0301': 'jungle',
    '0401': 'jungle',
    '0501': '',
    '0601': '',
    '0701': 'beach',
    ## 2
    '0002': 'jungle',
    '0102': 'jungle',
    '0202': 'jungle',
    '0302': 'jungle',
    '0402': 'jungle',
    '0502': '',
    '0602': '',
    '0702': 'beach',
    '0802': 'beach',
    ## 3
    '0003': 'jungle',
    '0103': 'jungle',
    '0203': 'jungle',
    '0303': 'jungle',
    '0403': 'jungle',
    '0503': 'jungle',
    '0603': 'jungle',
    '0703': '',
    '0803': 'beach',
    '0803': 'jungle',
    ## 4
    '0004': 'jungle',
    '0104': 'jungle',
    '0204': 'jungle',
    '0304': 'jungle',
    '0404': 'jungle',
    '0504': '',
    '0604': '',
    '0704': 'jungle',
    '0804': '',
    '0904': 'beach',
    ## 5
    '0005': '',
    '0105': 'jungle',
    '0205': 'jungle',
    '0305': 'jungle',
    '0405': 'jungle',
    '0505': 'jungle',
    '0605': '', # Fighter
    '0705': '', # Fighter
    '0805': 'jungle',
    '0905': 'beach',        
    '1005': 'beach',
    ## 6
    '0006': 'jungle',
    '0106': 'jungle',
    '0206': 'jungle',
    '0306': 'jungle',
    '0406': 'hill',
    '0506': 'jungle',
    '0606': '',
    '0706': '',
    '0806': '',
    '0906': 'jungle',
    '1006': 'beach',
    ## 7
    '0007': 'jungle',
    '0107': 'jungle',
    '0207': 'jungle',
    '0307': 'hill',
    '0407': 'jungle',
    '0507': 'hill',
    '0607': 'jungle',
    '0707': '',
    '0807': '',
    '0907': '',
    '1007': 'beach',
    ## 8
    '0008': 'jungle',
    '0108': 'jungle',
    '0208': 'jungle',
    '0308': 'jungle',
    '0408': 'jungle',
    '0508': 'jungle',
    '0608': 'jungle',
    '0708': '', # Henderson
    '0808': '',
    '0908': '', # NB NE
    '1008': 'beach', # NB SW
    '1008': '', # NB SW
    '1108': 'beach',
    ## 9
    '0009': 'jungle',
    '0109': 'jungle',
    '0209': '',
    '0309': 'jungle',
    '0409': 'jungle',
    '0509': 'jungle',
    '0609': 'jungle',
    '0709': '', # Henderson
    '0809': 'jungle',
    '0909': '', # Bridge
    '1009': 'beach',
    '1109': 'beach',
    '1209': 'beach',
    ## 10
    '0010': 'jungle',
    '0110': '',
    '0210': 'jungle',
    '0310': 'jungle',
    '0410': 'jungle',
    '0510': '',
    '0610': '',
    '0710': 'jungle',
    '0810': 'jungle',
    '0910': '',
    '1010': 'beach',
    '1110': 'beach',
    ## 11
    '0011': 'jungle',
    '0111': 'jungle',
    '0211': 'jungle',
    '0311': 'jungle',
    '0411': 'jungle',
    '0511': 'jungle',
    '0611': 'jungle',
    '0711': 'jungle',
    '0811': '',
    '0911': '',
    '1011': 'beach',
    '1111': 'beach',
    ## 12
    '0012': 'jungle',
    '0112': 'jungle',
    '0212': '',
    '0312': 'jungle',
    '0412': 'jungle',
    '0512': 'jungle',
    '0612': 'jungle',
    '0712': 'jungle',
    '0812': '',
    '0912': 'beach',
    ## 13
    '0013': 'jungle',
    '0113': 'jungle',
    '0213': 'jungle',
    '0313': 'jungle',
    '0413': 'jungle',
    '0513': 'jungle',
    '0613': 'jungle',
    '0713': '',
    '0813': 'beach',
    '0913': 'beach',
    ## 14
    '0014': 'jungle',
    '0114': 'jungle',
    '0214': 'jungle',
    '0314': 'jungle',
    '0414': 'jungle',
    '0514': 'jungle',
    '0614': 'jungle',
    '0714': 'beach',
    ## 15
    '0015': 'jungle',
    '0115': 'jungle',
    '0215': 'jungle',
    '0315': 'jungle',
    '0415': 'jungle',
    '0515': 'jungle',
    '0615': 'jungle',
    '0715': 'beach',
    ## 16
    '0016': 'jungle',
    '0116': 'jungle',
    '0216': 'jungle',
    '0316': 'jungle',
    '0416': 'jungle',
    '0516': 'jungle',
    '0616': 'jungle',
    '0716': 'beach',
    ## 17
    '0017': 'hill',
    '0117': 'jungle',
    '0217': 'jungle',
    '0317': 'jungle',
    '0417': 'jungle',
    '0517': 'jungle',
    '0617': '',
    '0717': 'beach',
    ## 18
    '0018': 'hill',
    '0118': 'jungle',
    '0218': 'jungle',
    '0318': 'jungle',
    '0418': 'jungle',
    '0518': '',
    '0618': 'beach',
    '0618': '', # NB
    '0718': 'beach',
    '0718': 'jungle',
    ## 19
    '0019': 'jungle',
    '0119': 'jungle',
    '0219': 'jungle',
    '0319': 'jungle',
    '0419': 'jungle',
    '0519': '',
    '0619': 'beach',
    '0619': 'jungle',
    ## 20
    '0020': 'jungle',
    '0120': 'jungle',
    '0220': '',
    '0320': '',
    '0420': 'jungle',
    '0520': '',
    '0620': 'beach',
    '0620': 'jungle',
    '0121': '',
    '0321': '',
    '0521': '',
    '0721': 'beach',
}

# --------------------------------------------------------------------
river_cross = [
    # Tenaru
    ["0400", "0500"],
    ["0400", "0501"],
    ["0401", "0501"],
    ["0401", "0502"],
    ["0401", "0402"],
    ["0302", "0402"],
    ["0302", "0303"],
    ["0202", "0303"],
    ["0203", "0303"],
    ["0203", "0304"],
    ["0203", "0204"],
    ["0203", "0104"],
    ["0103", "0104"],
    ["0003", "0104"],
    ["0003", "0004"],
    # East of ilu
    ["0701", "0702"],
    ["0701", "0601"],
    ["0600", "0601"],
    # Ilu
    ["0503", "0603"],
    ["0602", "0603"],
    ["0703", "0603"],
    ["0703", "0704"],
    ["0703", "0803"],
    # East of Lunga
    ["1006", "0907"],
    ["1006", "1007"],
    # Lunga
    ["0008", "0009"],
    ["0008", "0109"],
    ["0108", "0109"],
    ["0108", "0208"],
    ["0207", "0208"],
    ["0308", "0208"],
    ["0308", "0309"],
    ["0308", "0408"],
    ["0407", "0408"],
    ["0508", "0408"],
    ["0508", "0509"],
    ["0608", "0509"],
    ["0608", "0609"],
    ["0709", "0609"],
    ["0709", "0609"],
    ["0709", "0710"],
    ["0809", "0710"],
    ["0809", "0810"],
    ["0809", "0910"],
    ["0909", "0910"],
    ["0909", "1009"],# East branch
    ["1008", "1009"],
    ["1008", "1109"],
    ["1108", "1109"],
    ["1009", "0910"],# Middle branch
    ["1009", "1010"],
    ["1009", "1110"],
    ["1109", "1110"],
    ["1110", "1111"],#Top middle branch
    ["1110", "1010"],
    ["0919", "1010"],# West branch
    ["1010", "0911"],
    ["1010", "1011"],
    ["1010", "1111"],
    ["1111", "1011"],# West branch top
    # West of Lunga
    ["0811", "0912"],
    ["0911", "0912"],
    ["1011", "0912"],
    # Matanikais
    ["0420", "0521"],
    ["0520", "0521"],
    ["0520", "0620"],
    ["0619", "0620"]
]

# --------------------------------------------------------------------
ridge_edge = [
    # Bloody ridge
    ["0206", "0307"],
    ["0306", "0307"],
    ["0306", "0406"],
    ["0405", "0406"],
    ["0506", "0406"],
    ["0506", "0507"],
    ["0606", "0507"],
    ["0307", "0207"],
    ["0307", "0308"],
    ["0307", "0407"],
    ["0406", "0407"],
    ["0507", "0407"],
    ["0507", "0508"],
    ["0507", "0607"],
    # Mt. Austen
    ["0016", "0017"],
    ["0117", "0017"],
    ["0017", "0118"],
    ["0118", "0018"],
    ["0018", "0119"],
    ["0018", "0019"],
]

# --------------------------------------------------------------------
def pretty_name(pn):
    spl = pn.split()
    if spl[0] not in ['usa', 'usaf', 'usmc', 'ijn', 'ijaaf', 'ija']:
        return ' '.join(spl)
    
    spl[0] = spl[0].upper()

    pn_map = {'ishi': 'Ishi Battalion',
              'kuma': 'Kuma Battalion',
              'mar' : '5th Marines Battalion',
              'labour':  'Labour Battalion',
              'dd':   'Destroyer',
              'g4m1': 'G4M "Betty"',
              'a6m2': 'A6M "Zero"',
              'd3a2': 'D3A1 "Val"',
              'f4f':  'F4F "Wildcat"',
              'p400': 'P400 "Air-Cobra"',
              'sdb':  'SDB "Dauntless"',
              'tbf':  'TBF "Avenger"',
              'p38':  'P38 "Lightning"'}
    pn2 = pn_map.get(spl[1],None)
    if pn2 is not None:
        return spl[0]+' '+pn2
    
    suf = lambda n : ('th' if n in [11,12,13] else
                      'st' if n % 10 == 1 else
                      'nd' if n % 10 == 2 else
                      'rd' if n % 10 == 3 else 'th')
    try:
        div    = int(spl[1])
        spl[1] = str(div)+suf(div)+' Division'
    except:
        if spl[1] == 'am':
            spl[1] = 'Americal Division'

    if len(spl) >= 3 and spl[1] == 'inf':
        bat    = int(spl[2])
        spl[1] = str(bat)+suf(bat)
        spl[2] = 'Infantry Battalion'
    if len(spl) >= 3 and spl[1] == 'tank':
        bat    = int(spl[2])
        spl[1] = str(bat)+suf(bat)
        spl[2] = 'Armoured Battalion'

    typ_map = {'inf': 'Infantry',
               'tank': 'Armoured',
               'art':  'Artillery',
               'def':  'Defensive',
               'anti': 'Anti',
               'eng':  'Engineer',
               'ab':   'Air-borne',
               'com':  'Raider',
               'mot':  'Motorised'}
    typ = typ_map.get(spl[2],None) if len(spl) >= 3 else None
    # print(f'{spl[2]} -> {typ}')
    
    if len(spl) >= 4 and typ is not None:
        spl[2] = typ
        if spl[3] == 'mg':
            spl[2] = 'Machine Gun'
            spl[3] = 'Battalion'
        elif spl[3] == 'tank':
            spl[3] = 'Armour'
        elif spl[3] == 'air':
            spl[3] = 'Air'
        elif spl[3] == 'inf':
            spl[3] = 'Battalion'
        elif spl[3] == 'sly':
            spl[3] = 'Sly Battalion'
        elif spl[3] == 'art':
            spl[3] = 'Artillery'
        else:
            light = False
            if spl[3].endswith('lt'):
                spl[3] = spl[3].replace('lt','')
            bat    = int(spl[3])
            spl[2] = str(bat)+suf(bat)
            spl[3] = f'{typ} {"Light " if light else ""}Battalion'

    if len(spl) >= 5:
        spl[3] = spl[3].replace('Battalion','Regiment')
        try:
            bat    = int(spl[4])
            spl[4] = str(bat)+suf(bat)+' Battalion'
        except:
            pass 

    return ' '.join(spl)

# --------------------------------------------------------------------
def turnNo(traits):
    left = Trait.findTrait(traits,MarkTrait.ID,key='name',value='left')
    if not left:
        left = Trait.findTrait(traits,MarkTrait.ID,
                               key='name',value='upper left')
    if not left:
        return -1
    
    raw  = left['value'].replace('fb setup=','')
    try:
        return int(raw)
    except:
        pass
    
    return -1
# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,verbose=False):
    from pathlib import Path
    
    Verbose().setVerbose(verbose)
    verb = VerboseGuard()

    
    # ----------------------------------------------------------------
    updateHex                = key(NONE,0)+',updateHex'
    checkCombat              = key(NONE,0)+',checkCombat'
    publishDRM               = key(NONE,0)+',publishDRM'
    printKey                 = key(NONE,0)+',print'
    checkSetup               = key(NONE,0)+',checkSetup'
    reinforceKey             = key(NONE,0)+',reinforce'
    countAirstrips           = key(NONE,0)+',countAirfields'
    checkSupply              = key(NONE,0)+',checkSupply'
    nextPhase                = key('T',ALT)
    hidden                   = LaTeXExporter.Specials.HIDDEN_NAME
    battleCtrl               = LaTeXExporter.Specials.BATTLE_CTRL
    battleCalc               = LaTeXExporter.Specials.BATTLE_CALC
    battleUnit               = LaTeXExporter.Specials.BATTLE_UNIT
    battleMarker             = LaTeXExporter.Specials.BATTLE_MARK
    debug                    = LaTeXExporter.Globals.DEBUG
    verbose                  = LaTeXExporter.Globals.VERBOSE
    currentBattle            = LaTeXExporter.Globals.CURRENT_BATTLE
    battleNo                 = LaTeXExporter.Globals.BATTLE_NO
    battleResult             = LaTeXExporter.Globals.BATTLE_RESULT
    battleShift              = LaTeXExporter.Globals.BATTLE_SHIFT
    battleOdds               = LaTeXExporter.Globals.BATTLE_ODDS
    battleFrac               = LaTeXExporter.Globals.BATTLE_FRAC
    battleAF                 = LaTeXExporter.Globals.BATTLE_AF
    battleDF                 = LaTeXExporter.Globals.BATTLE_DF
    calcOddsKey              = LaTeXExporter.Keys.CALC_BATTLE_ODDS
    calcFracKey              = LaTeXExporter.Keys.CALC_BATTLE_FRAC
    calcShift                = LaTeXExporter.Keys.CALC_BATTLE_SHFT
    rollDie                  = LaTeXExporter.Keys.ROLL_DICE
    diceKey                  = LaTeXExporter.Keys.DICE_KEY
    getBattle                = LaTeXExporter.Keys.GET_BATTLE
    curBtl                   = (f'{{{battleNo}=={currentBattle}&&'
                                f'{battleUnit}==true}}')
    curDef                   = (f'{{{battleNo}=={currentBattle}&&'
                                f'{battleUnit}==true&&'
                                f'IsAttacker==false}}')
    curAtt                   = (f'{{{battleNo}=={currentBattle}&&'
                                f'{battleUnit}==true&&'
                                f'IsAttacker==true}}')

    # ----------------------------------------------------------------
    game = build.getGame()

    # ----------------------------------------------------------------
    # Documentation 
    doc  = game.getDocumentation()[0]
    doc.addHelpFile(title='More information',fileName='help/more.html')
    with open('help.html','r') as mf:
        moreHelp = ''.join(mf.readlines())
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))

    imgs = ['.imgs/vassal-declare.png',
            '.imgs/vassal-resolve.png']
    for img in imgs:
        src = Path(img)
        dst = Path('images') / src.name
        vmod.addExternalFile(img,str(dst))

    # ----------------------------------------------------------------
    # Set-up
    setups = game.addPredefinedSetup(name='Set-ups',
                                     description='Set-ups',
                                     isMenu=True)
    setups.addPredefinedSetup(name='No set-up',
                              file=None,
                              isMenu=False,
                              description='Start from scratch')
    setups.addPredefinedSetup(name='Example US and IJ set-up',
                              file='Example.vsav',
                              useFile=True,
                              isMenu=False,
                              description='Example US and IJ setup')
    vmod.addExternalFile('Example.vsav','Example.vsav')
    
    # --- Remove piece window - not needed ---------------------------
    pwindows = game.getPieceWindows()
    for pw in pwindows.values():
        print(f'Piece window: {pw["name"]}')
        tab = pw.getTabs().get("Counters",None)
        if not tab:
            continue

        panels = tab.getPanels()
        union  = panels.get("US",None)
        conf   = panels.get("IJ",None)
        if union:
            tab.remove(union)
        if conf:
            tab.remove(conf)
        # print(f'Lists: {pw.getLists()}')
        # print(f'Tabs: {pw.getTabs()}')
        # print(f'Combos: {pw.getCombos()}')
        # print(f'Panels: {pw.getPanels()}')
        # game.remove(pw)
    
    # ----------------------------------------------------------------
    maps = game.getMaps()
    main = maps['Board']

    mkeys = main.getMassKeys()
    mkeys['Eliminate']['icon'] = 'eliminate-icon.png'
    mkeys['Flip']     ['icon'] = 'flip-icon.png'

    pool = maps['DeadMap']
    pool['icon'] = 'pool-icon.png'
    pool.getMassKeys()['Restore']['icon'] = 'restore-icon.png'

    game.getChartWindows()['OOBs']['icon'] = 'oob-icon.png'
    
    # --- Get Zoned area ---------------------------------------------
    zoned = main.getBoardPicker()[0].getBoards()['Board'].getZonedGrids()[0]
    
    # ----------------------------------------------------------------
    # Set up some global properties
    ls            = lambda s : s #.lstrip('0')
    jungles       = 'Jungle'
    rivers        = 'Rivers'
    ridges        = 'Ridges'
    autoClear     = 'AutoStrategic'
    autoReinforce = 'AutoReinforce'
    autoSupply    = 'AutoSupply'
    jungleList = [ls(hx) for hx,ter in cbt_terrain.items() if ter == 'jungle']
    jungleCode = ':'+':'.join(jungleList)+':'
    riverList  = [[ls(p[0]),ls(p[1])] for p in river_cross]
    ridgeList  = [[ls(p[0]),ls(p[1])] for p in ridge_edge]  
    riverList.extend([[ls(p[1]),ls(p[0])] for p in river_cross])
    ridgeList.extend([[ls(p[1]),ls(p[0])] for p in ridge_edge])
    riverCode  = ':'+':'.join(['@'.join(p) for p in riverList])+':'
    ridgeCode  = ':'+':'.join(['@'.join(p) for p in ridgeList])+':'

    # ----------------------------------------------------------------
    print('Module:',game['name'])
    go = game.getGlobalOptions()[0]
    go.addBoolPreference(name    = autoClear,
                         default = True,
                         desc    = 'Automatically move IJAAF, IJN, and USAF',
                         tab     = game['name'])
    go.addBoolPreference(name    = autoReinforce,
                         default = True,
                         desc    = 'Automatically stage reinforcements',
                         tab     = game['name'])
    go.addBoolPreference(name    = autoSupply,
                         default = True,
                         desc    = 'Automatically check IJ supply',
                         tab     = game['name'])
    bools = go.getBoolPreferences()
    bools['wgAutoOdds']   ['default'] = True
    bools['wgAutoResults']['default'] = True
                         
    # ----------------------------------------------------------------
    # Global properties
    notSetup    = 'NotInitialSetup'
    notDeclare  = 'NotDeclare'
    notResolve  = 'NotResolve'
    notAir      = 'NotAir'
    notSupply   = 'NotSupply'
    
    glob = game.getGlobalProperties()[0]
    glob.addProperty(name =jungles, initialValue=jungleCode, isNumeric=False)
    glob.addProperty(name =rivers,  initialValue=riverCode,  isNumeric=False)
    glob.addProperty(name =ridges,  initialValue=ridgeCode,  isNumeric=False)
    glob.addProperty(name         = notSetup,
                     initialValue = False,
                     description  = 'Whether we are not at the start')
    glob.addProperty(name         = notResolve,
                     initialValue = True,
                     description  = 'Whether we can resolve')
    glob.addProperty(name         = notDeclare,
                     initialValue = True,
                     description  = 'Whether we can declare')
    glob.addProperty(name         = notAir,
                     initialValue = True,
                     description  = 'Whether we can declare')
    glob.addProperty(name         = notSupply,
                     initialValue = True,
                     description  = 'Whether we can declare')
    glob.addProperty(name='TurnTracker.defaultFontBold',
                     initialValue = "true")
    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    zones  = zoned.getZones()
    hzone  = zones['hex']
    hgrids = hzone.getHexGrids()
    hgrid  = hgrids[0]
    hnum   = hgrid.getNumbering()[0]
    hnum['visible']  = False
    hgrid['visible'] = False
    
    # Move hex to the bottom of the list (processed first!)
    zoned.remove(hzone)
    zoned.append(hzone)

    # ----------------------------------------------------------------
    # Reorder Hendeson and Fighter zones
    hfzone = zones['Henderson']
    hpzone = zones['Henderson pool']
    ffzone = zones['Fighter']
    fpzone = zones['Fighter pool']
    zoned.remove(hfzone);
    zoned.remove(ffzone);
    zoned.insertBefore(hfzone,hpzone)
    zoned.insertBefore(ffzone,fpzone)

    # ----------------------------------------------------------------
    # Layers
    pieceLayer      = 'PieceLayer'
    unitLayer       = 'Units'
    btlLayer        = 'Units' #'Battle' - doesn't work with auto odds
                              #if not same as units - anyways, the
                              #markers should stack with units.
    oddLayer        = 'Odds'
    resLayer        = 'Result'
    airLayer        = 'Airfield'

    layerNames = {airLayer:  {'t': airLayer+' markers', 'i': ''},
                  unitLayer: {'t': unitLayer,           'i': ''},
                  oddLayer:  {'t': oddLayer+' markers', 'i': ''},
                  resLayer:  {'t': resLayer+' markers', 'i': ''} }
                  
    layers = main.addLayers(description='Layers',
                            layerOrder=layerNames)
    for lt,ln in layerNames.items():
        layers.addControl(name       = lt,
                          tooltip    = f'Toggle display of {ln["t"]}',
                          text       = f'Toggle {ln["t"]}',
                          icon       = ln['i'],
                          layers     = [lt])
    layers.addControl(name    = 'Show all',
                      tooltip = f'Show all',
                      text    = f'Show all',
                      command = LayerControl.ENABLE,
                      layers  = list(layerNames.keys()))

    lmenu = main.addMenu(description = 'Toggle layers',
                         text        = '',
                         tooltip     = 'Toggle display of layers',
                         icon        = 'layer-icon.png',
                         menuItems   = ([f'Toggle {ln["t"]}'
                                         for ln in layerNames.values()]
                                        +['Show all']))

    hideP  = main.getHidePiecesButton()
    if len(hideP) > 0:
        print('Found hide pieces button, moving layers before that')
        main.remove(layers)
        main.remove(lmenu)
        main.insertBefore(layers,hideP[0])
        main.insertBefore(lmenu, hideP[0])
    else:
        print('Hide pueces button not found',hideP)

    # ----------------------------------------------------------------
    turns = game.getTurnTracks()['Turn']
    phases = turns.getLists()['Phase']
    phases['list'] = ','.join(['Setup',
                               'IJ Reinforcements',
                               'IJ Movement',
                               'IJ Combat',
                               'IJ Supply',
                               'IJ Strategic',
                               'US Reinforcements',
                               'US Movement',
                               'US Combat',
                               'US Engineering',
                               'US Air'])

    turns.addHotkey(hotkey   = checkSetup,
                    match    = '{Turn==1}',
                    name     = 'Check for setup phase')
    turns.addHotkey(hotkey   = nextPhase,
                    match    = '{Phase.contains("Setup")&&Turn>1}',
                    name     = 'Skip setup phase')

    # --- Clear markers phases ---------------------------------------
    # Do not clear upon entering combat phase! 
    keys = turns.getHotkeys(asdict=True)
    keys['Clear battle markers']['match'] = f'{{!Phase.contains("Combat")}}'
    
    # ----------------------------------------------------------------
    # prototypes = game.getPrototypes()[0]
    # ----------------------------------------------------------------
    prototypeContainer = game.getPrototypes()[0]
    # prototypes         = prototypeContainer.getPrototypes()
    

    # ----------------------------------------------------------------
    # Global keys 
    globkey = [
        {
            'name':   'IJN/IJAFF Attack -> eliminated',
            'named':  'RetireIJ',
            'phase':  'IJ Strategic',
            'key':    key('E',CTRL_SHIFT),
            'filter': ('(LocationName=="IJN Attack pool"'+ 
                       '|| LocationName=="IJAAF Attack pool"'
                       '|| CurrentZone=="hex")'), 
            'proto': []
        },
        {
            'name':   'IJN/IJAFF Reserve -> Attack',
            'named':  'ShiftIJ',
            'phase':  'IJ Strategic',
            'key':    key('A',CTRL_SHIFT),
            'filter': ('(LocationName=="IJN Reserve pool"'+ 
                       '|| LocationName=="IJAAF Reserve pool")'),
            'proto':  ['IJN Attack','IJAAF Attack']
        },
        {
            'name':   'USAF airstrip -> Reserve',
            'named':  'RestoreUSAF',
            'phase':  'US Air',
            'key':    key('Q',CTRL_SHIFT),
            'filter': ('(CurrentZone=~"Henderson.*"'
                       '||CurrentZone=~"Fighter.*")'),
            'proto':  ['USAF Reserve']
        },
        {
            'name':   'USAF refuel',
            'named':  'RefuelUSAF',
            'phase':  'US Air',
            'key':    key('1',CTRL_SHIFT),
            'filter': ('(LocationName=="USAF CAP pool"'+ 
                       '|| LocationName=="USAF Ground Attack pool"'
                       '|| CurrentZone=="hex")'),
            'proto': []
        }]
    
    # ----------------------------------------------------------------
    # Loop over global keys and add turn keys and normal global keys.
    # Also add relevant prototypes
    for g in globkey:
        p = g['phase']
        f = g['filter']
        n = key(NONE,0)+','+g['named']
        w = g['phase']
        turns.addHotkey(hotkey       = n,
                        match        = f'{{Phase=="{p}"&&{autoClear}}}',
                        reportFormat = f'{{{verbose}?("! {g["name"]}"):""}}',
                        name         = g['name'])
        main.addMassKey(name         = g['name'],
                        buttonHotkey = n,
                        hotkey       = g['key'],
                        buttonText   = '', # g['name']
                        target       = '',
                        filter       = f'{{Phase=="{w}"&&{f}}}',
                        reportFormat = f'{{{debug}?("~ {g["name"]} (global)"):""}}',
                        reportSingle = True)
        # If global key had prototypes, define those to 
        for pn in g.get('proto',[]):
            traits = [ReportTrait(g['key']),
                      SendtoTrait(mapName     = 'Board',
                                  boardName   = 'Board',
                                  name        = f'Send to {pn}',
                                  restoreName = '',
                                  restoreKey  = '',
                                  zone        = f'{pn} pool',
                                  destination = 'Z', #R for region
                                  key         = g['key'],
                                  x           = 0,
                                  y           = 0),
                      BasicTrait()]
            prototypeContainer.addPrototype(
                name        = f'{pn} prototype',
                description = f'Send to {pn} prototype',
                traits      = traits)
            
    # ----------------------------------------------------------------
    turns.addHotkey(hotkey       = checkCombat,
                    reportFormat = f'{{{debug}?("~ Check for combat (turn)"):""}}',
                    name         = 'Check combat')
    main.addMassKey(name         = 'Check combat',
                    buttonHotkey = checkCombat,
                    hotkey       = checkCombat,
                    buttonText   = '', # g['name']
                    target       = '',
                    filter       = f'{{BasicName=="game turn"}}',
                    reportFormat = f'{{{debug}?("~ Check for combat (global)"):""}}',
                    reportSingle = False)
    traits = [
        GlobalPropertyTrait(
            ['',checkCombat,GlobalPropertyTrait.DIRECT,
             f'{{!Phase.contains("Combat")&&!Phase.contains("Movement")}}'],
            name        = notDeclare,
            numeric     = True,
            description = 'Can we declare combats'),
        GlobalPropertyTrait(
            ['',checkCombat,GlobalPropertyTrait.DIRECT,
             f'{{!Phase.contains("Combat")}}'],
            name        = notResolve,
            numeric     = True,
            description = 'Can we resolve combats'),
        GlobalPropertyTrait(
            ['',checkCombat,GlobalPropertyTrait.DIRECT,
             f'{{!Phase.contains("US Air")}}'],
            name        = notAir,
            numeric     = True,
            description = 'Can we resolve combats'),
        GlobalPropertyTrait(
            ['',checkCombat,GlobalPropertyTrait.DIRECT,
             f'{{!Phase.contains("IJ Supply")}}'],
            name        = notSupply,
            numeric     = True,
            description = 'Can we resolve combats'),
        ReportTrait(
            checkCombat,
            report = (f'{{{debug}?("~ Check for combat => "+'
                      f'" {notAir}="+{notAir}+'
                      f'" {notSupply}="+{notSupply}+'
                      f'" {notResolve}="+{notResolve}+'
                      f'" {notDeclare}="+{notDeclare}):""}}')),
            BasicTrait()]
    prototypeContainer.addPrototype(
        name        = f'Check combat prototype',
        description = f'Check for combat phases',
        traits      = traits)
    
    
    # ----------------------------------------------------------------
    traits = [
        # PrototypeTrait(name=battleUnit),
        MarkTrait(name='DF',          value=0),
        MarkTrait(name='CF',          value=0),
        MarkTrait(name='MF',          value=0),
        MarkTrait(name='EffectiveDF', value=0),
        MarkTrait(name='EffectiveAF', value=0),
        CalculatedTrait(name='OddsShift',
                        expression=f'{{Support+{battleShift}}}'),
        ReportTrait(
            calcShift,
            report=(f'{{{verbose}?("! "+BasicName+" adds "+'
                    f'Support+" to DRM -> "+{battleShift}):""}}')),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(
        name        = f'Ground support prototype',
        description = f'Ground support unit',
        traits      = traits)

    # ----------------------------------------------------------------
    keys                                = main.getMassKeys()
    userMark                 = keys['User mark battle']
    selMark                  = keys['Selected mark battle']
    resMark                  = keys['Selected resolve battle']
    userMark['canDisable']   = True
    userMark['propertyGate'] = notDeclare
    selMark ['canDisable']   = True
    selMark ['propertyGate'] = notDeclare
    resMark ['canDisable']   = True
    resMark ['propertyGate'] = notResolve
    
    main.addMassKey(
        name         = 'Store defender hex',
        buttonHotkey = updateHex,
        buttonText   = '',
        singleMap    = True,
        hotkey       = updateHex,
        target       = '',
        filter       = curDef,
        reportFormat = (f'{{{debug}?("~ Mass update defender hex"):""}}'))
    main.addMassKey(
        name         = 'Store attacker hex',
        buttonHotkey = updateHex,
        buttonText   = '',
        singleMap    = True,
        hotkey       = updateHex,
        target       = '',
        filter       = curAtt,
        reportFormat = (f'{{{debug}?("~ Mass update attacker hex"):""}}'))
    
        
    # ----------------------------------------------------------------
    traits = [
        CalculatedTrait(
            name       = 'RealRange',
            expression = '{Range-1}'),
        AreaTrait(name              = 'ArtilleryRange',
                  transparancyColor = rgb(176,49,63),
                  activateCommand   = 'Toggle artillery range',
                  fixedRadius       = False,
                  radiusMarker      = 'RealRange',
                  description       = 'Show artillery range'),
        BasicTrait()]
    prototypeContainer.addPrototype(
        name        = f'Artillery range prototype',
        description = f'Show artillery range unit',
        traits      = traits)

    # Send Ctrl-A to selected units 
    main.addMassKey(
        name = 'Toggle artillery range',
        buttonHotkey = key('A'),
        hotkey       = key('A'),
        icon         = '')
        
        
    
    # ----------------------------------------------------------------
    # Define global keys for turn marker
    turns.addHotkey(hotkey       = key('F',CTRL_SHIFT),
                    match        = '{Phase=="IJ Reinforcements"&&Turn>1}',
                    reportFormat = '--- IJ Turn ---',
                    name         = 'IJ Turn')
    turns.addHotkey(hotkey       = key('F',CTRL_SHIFT),
                    match        = '{Phase=="US Reinforcements"}',
                    reportFormat = '--- US Turn ---',
                    name         = 'US Turn')
    main.addMassKey(name         = 'Flip turn marker',
                    buttonHotkey = key('F',CTRL_SHIFT),
                    hotkey       = LaTeXExporter.Keys.FLIP_KEY,
                    buttonText   = '', 
                    target       = '',
                    filter       = ('{BasicName == "game turn"}'))
    turns.addHotkey(hotkey       = reinforceKey,
                    match        = ('{AutoReinforce&&'
                                    '(Phase.contains("Reinforcements")||'
                                    '(Phase.contains("Setup")&&Turn==1))}'),
                    reportFormat = f'{{{verbose}?("Reinforcing"):""}}',
                    name         = 'Reinforcements')
    main.addMassKey(name         = 'Reinforcements',
                    buttonHotkey = reinforceKey,
                    hotkey       = reinforceKey,
                    buttonText   = '',
                    target       = '',
                    filter       = '{CurrentBoard.contains("OOB")}',
                    singleMap    = False)
    game.addStartupMassKey(
        name        = 'Initial setup',
        hotkey      = reinforceKey,
        target      = '',
        filter      = f'{{CurrentBoard.contains("OOB")}}',
        whenToApply = StartupMassKey.START_GAME,
        singleMap   = False,
        reportFormat=f'{{{verbose}?("Prepare set-up units"):""}}')
    
    turnp = []
    for t in range(1,16):
        k = key(NONE,0)+f',Turn{t}'
        turns.addHotkey(hotkey       = k,
                        match        = (f'{{Turn=={t}&&'
                                        'Phase=="IJ Reinforcements"}'),
                        reportFormat = f'=== Turn {t} ===',
                        name         = f'Turn {t}')
        main.addMassKey(name         = f'Turn marker to {t}',
                        buttonHotkey = k,
                        hotkey       = k,
                        buttonText   = '', 
                        target       = '',
                        filter       = ('{BasicName == "game turn"}'))
        pn     = f'To turn {t}'
        traits = [ SendtoTrait(mapName     = 'Board',
                               boardName   = 'Board',
                               name        = '',
                               restoreName = '',
                               restoreKey  = '',
                               zone        = f'turn track',
                               destination = SendtoTrait.REGION,
                               region      ='game turn' if t == 1 else f'T{t}',
                               key         = k,
                               x           = 0,
                               y           = 0),
                   BasicTrait()]
        prototypeContainer.addPrototype(
            name        = f'{pn} prototype',
            description = f'{pn} prototype',
            traits      = traits)
        turnp.append(pn)

    # ----------------------------------------------------------------
    prototypes         = prototypeContainer.getPrototypes()
    toRemove           = []
    mapPrototypes      = {'armoured reconnaissance': 'reconnaisance',
                          'infantry machine gun':    'infantry',
                          'armoured amphibious':     'armoured',
                          'amphibious':              '',
                          'artillery mountain':      'artillery',
                          'machine gun':             '',
                          'infantry air defence':    'infantry',
                          'artillery air defence':   'artillery',
                          'infantry airborne':       'infantry',
                          'fixed wing F B':          '',
                          'air defence':             '',
                          'air':                     ''}
    mapKeys = [f'{p} prototype' for p in mapPrototypes.keys()]
    for p,pp in prototypes.items():
        if p in mapKeys:
            toRemove.append(pp)
    #for p in toRemove:
    #    prototypeContainer.remove(p)
    prototypes         = prototypeContainer.getPrototypes()

    # ----------------------------------------------------------------
    ijCrt = { '1:2': ['AE', 'AE', 'A2', 'A1', 'A1', 'EX'],
              '1:1': ['AE', 'A2', 'A1', 'A1', 'EX', 'D1'],
              '2:1': ['A1', 'A1', 'EX', 'EX', 'D1', 'D1'],
              '3:1': ['EX', 'EX', 'EX', 'EX', 'D1', 'DE'] }
    usCrt = { '1:2': ['AE', 'A1', 'A1', 'EX', 'EX', 'DR'],
              '1:1': ['AE', 'A1', 'EX', 'EX', 'DR', 'D1'],
              '2:1': ['A1', 'EX', 'EX', 'D1', 'D1', 'DE'],
              '3:1': ['EX', 'EX', 'D1', 'D1', 'DE', 'DE'] }
    # ----------------------------------------------------------------
    combatDRM         = 'CombatDRM'
    defenderHex       = 'DefenderHex'
    defenderRiver     = 'DefenderRiver'
    defenderRidge     = 'DefenderRidge'
    defenderJungle    = 'DefenderJungle'
    hexesRange        = list(range(1,7))
    ln                = 'LocationName.toString()'
    dn                = lambda no : f'{defenderHex}{no}.toString()'
    dr                = lambda no : f'{defenderRiver}{no}.toString()'
    de                = lambda no : f'{defenderRidge}{no}.toString()'
    for no in hexesRange:
        glob.addProperty(name         = defenderHex+str(no),
                         initialValue = '-',
                         description  = 'Defender hex')
        glob.addProperty(name         = defenderHex+str(no)+'X',
                         initialValue = -1,
                         isNumeric    = True,
                         description  = 'Defender hex X pixel')
        glob.addProperty(name         = defenderHex+str(no)+'Y',
                         initialValue = -1,
                         isNumeric    = True,
                         description  = 'Defender hex Y pixel')
        glob.addProperty(name         = defenderRiver+str(no),
                         initialValue = '-',
                         description  = 'Defender behind river hex')
        glob.addProperty(name         = defenderRidge+str(no),
                         initialValue = '-',
                         description  = 'Defender behind ridge hex')
    glob.addProperty(name         = defenderJungle,
                     initialValue = False,
                     description  = 'Defender in jungle hex')
    glob.addProperty(name         = combatDRM,
                     initialValue = 0,
                     description  = 'Die-roll modifier')
        
    repDHexes = '+","+'.join([f'{dn(no)}' for no in hexesRange])
    repDCoord = '+","+'.join([f'"("+{defenderHex}{no}X+","+{defenderHex}{no}Y+")"'
                              for no in hexesRange])
    repDRiver = '+","+'.join([f'{dr(no)}' for no in hexesRange])
    repDRidge = '+","+'.join([f'{de(no)}' for no in hexesRange])
    #repLRiver = '+","+'.join([f'{h}' for h in rivers])
    #repLRidge = '+","+'.join([f'{h}' for h in ridges])
    traits = [
        CalculatedTrait(
            name       = 'HasRange',
            expression = f'{{Range!=""}}'),
        TriggerTrait(
            name       = '',
            key        = updateHex,
            actionKeys = [f'{updateHex}{no}Defender' for no in hexesRange],
            property   = '{!IsAttacker}'),
        TriggerTrait(
            name       = '',
            key        = updateHex,
            actionKeys = [f'{updateHex}{no}Attacker' for no in hexesRange],
            property   = '{IsAttacker}'),
        GlobalPropertyTrait(
            ['',f'{updateHex}1Defender',GlobalPropertyTrait.DIRECT,
             f'{{InJungle?-1:{defenderJungle}}}'],
            name        = defenderJungle,
            numeric     = True,
            description = 'Some defender in Jungle'),
        ReportTrait(
            updateHex,
            report = (f'{{{debug}?("~"+BasicName+" "+LocationName'
                      f'+" In jungle: "+InJungle'
                      f'+" (DRM: "+{defenderJungle}+")"'
                      f'+" Behind river: "+BehindRiver'
                      f'+" ridge: "+BehindRidge'
                      f'+" Hexes: "+{repDHexes}'
                      f'+" Rivers: " +{repDRiver}'
                      f'+" Ridges: " +{repDRidge}'
                      f'+""):""}}'))
    ]
    #
    # The next two traits checks (for the attacker), if the
    # atttacker is attacking over a river or ridge edge for
    # defender hex # `no'.  This is used below in the
    # GlobalPropertiesTrait's
    #
    # `@' separator
    edgeRiver = [
        CalculatedTrait(
            name        = f'RiverEdge{no}',
            expression  = (f'{{{rivers}.contains(":"+'
                           f'{ln}+"@"+{dn(no)}+":")}}'),
            description = 'Check river edge {no}')
        for no in hexesRange]
    edgeRidge = [
        CalculatedTrait(
            name        = f'RidgeEdge{no}',
            expression  = (f'{{{ridges}.contains(":"+'
                           f'{ln}+"@"+{dn(no)}+":")}}'),
            description = 'Check ridge edge {no}')
        for no in hexesRange]
    rnge   = [
        CalculatedTrait(
            name = f'Range{no}',
            expression = (f'{{({defenderHex}{no}X<0&&{defenderHex}{no}Y<0)?-1:'
                          f'Range({defenderHex}{no}X,{defenderHex}{no}Y)}}'))
        for no in hexesRange]
    remote = [
        CalculatedTrait(
            name        = f'IsRemote{no}',
            expression  = (f'{{HasRange&&Range{no}>1&&Range{no}<=Range}}'))
        for no in hexesRange]
           

    #
    # Defenders set the hexes they occupy
    #
    defHexes   = []
    defHexesX  = []
    defHexesY  = []
    repKHexes  = [f'{updateHex}{no}Defender' for no in hexesRange]
    repKRiver  = [f'{updateHex}{no}Attacker' for no in hexesRange]
    repERiver  = '+","+'.join([f'RiverEdge{no}' for no in hexesRange])
    repERidge  = '+","+'.join([f'RidgeEdge{no}' for no in hexesRange])
    repRange   = '+","+'.join([f'Range{no}'     for no in hexesRange])
    repRemote  = '+","+'.join([f'IsRemote{no}'  for no in hexesRange])
    for no in hexesRange:
        notthis    = '&&'.join([f'({dn(oth)}!={ln})'
                                for oth in hexesRange if oth != no])
        thisXY     = f'({dn(no)}=={ln})'
        thisEmpty  = f'{dn(no)}==""'
        #
        #
        # Store unique defender hexes - at most 6
        #
        defHexes.append(
            GlobalPropertyTrait(
                ['',f'{updateHex}{no}Defender',GlobalPropertyTrait.DIRECT,
                 f'{{({thisEmpty}&&{notthis}'
                 f'?{ln}:{dn(no)})}}'],
                name = f'{defenderHex}{no}',
                numeric = False,
                description = 'Update defender hex'))
        defHexesX.append(
            GlobalPropertyTrait(
                ['',f'{updateHex}{no}Defender',GlobalPropertyTrait.DIRECT,
                 f'{{({thisXY}?CurrentX:{defenderHex}{no}X)}}'],
                name = f'{defenderHex}{no}X',
                numeric = False,
                description = 'Update defender hex'))
        defHexesY.append(
            GlobalPropertyTrait(
                ['',f'{updateHex}{no}Defender',GlobalPropertyTrait.DIRECT,
                 f'{{({thisXY}?CurrentY:{defenderHex}{no}Y)}}'],
                name = f'{defenderHex}{no}Y',
                numeric = False,
                description = 'Update defender hex'))
        
    #
    # Check against all defender hexes if attacker is behind river or
    # ridge.  If attacker is, then set property value to defender's
    # hex (if not empty, meaning has previously been cleared).  If
    # not, set property value to the empty string.
    #
    # Note, we need to check that the attacker is not an artillery
    # unit providing remote fire support.  We check the range to the
    # defender's hex.  If the current attacker is an artillery unit
    # providing remote fire support, then we should _not_ change the
    # value of the global property.
    #
    # Also, check if attacker has any combat factors (ground support
    # units, like destroyers or fighters do no), and if not, leave the
    # current value as-is.
    #
    defRiver = [
        GlobalPropertyTrait(
            ['',f'{updateHex}{no}Attacker',GlobalPropertyTrait.DIRECT,
             f'{{(CF<=0||IsRemote{no})?{defenderRiver}{no}:'
             f'(RiverEdge{no}&&{dr(no)}!="")?{dn(no)}:""}}'],
            name        = f'{defenderRiver}{no}',
            numeric     = False,
            description = 'Update defender over river')
        for no in hexesRange]
    defRidge = [
        GlobalPropertyTrait(
            ['',f'{updateHex}{no}Attacker',GlobalPropertyTrait.DIRECT,
             f'{{(CF<=0||IsRemote{no})?{defenderRidge}{no}:'
             f'(RidgeEdge{no}&&{de(no)}!="")?{dn(no)}:""}}'],
            name        = f'{defenderRidge}{no}',
            numeric     = False,
            description = 'Update defender over ridge')
        for no in hexesRange]

    #
    # These two traits check if defender's current hex is in
    # `defenderRiver` (or `defenderRidge`).  That global has
    # previously been set by `{updateHex}{no}Attacker` to contain
    # defender hexes where _all_ attackers are behind a river (or
    # ridge).  Thus, if the defender's location is in the list of
    # checked river hexes, then it should be doubled on defence.
    #
    riverExpr  = '||'.join([f'{defenderRiver}{no}.contains({ln})'
                            for no in hexesRange])
    ridgeExpr  = '||'.join([f'{defenderRidge}{no}.contains({ln})'
                            for no in hexesRange])                           
    checkRiver = CalculatedTrait(
        name = 'BehindRiver',
        expression = f'{{{riverExpr}}}')
    checkRidge = CalculatedTrait(
        name = 'BehindRidge',
        expression = f'{{{ridgeExpr}}}')
        
    traits.extend(edgeRiver+
                  edgeRidge+
                  rnge+
                  remote+
                  defHexes+
                  defHexesX+
                  defHexesY+
                  defRiver+
                  defRidge+
                  [checkRiver,checkRidge]+
                  [ReportTrait(
                      *repKHexes,
                      report=(f'{{{debug}?("~ Setting defender hexes: "+'
                              f'{repDHexes}+" "+'
                              f'{repDCoord}):""}}')),
                   ReportTrait(
                       *repKRiver,
                       report=(f'{{{debug}?("~ "+BasicName+": "+HasRange+'
                               f'" Setting river "+{repDRiver}+'
                               f'" ridges "+{repDRidge}+'
                               f'" edge river "+{repERiver}+'
                               f'" edge ridge "+{repERidge}+'
                               f'" ranges "+{repRange}+'
                               f'" remote "+{repRemote}+'
                               #f'" (DBs "+{rivers}+'
                               #f'" & "+{ridges}+")"+'
                               f'""):""}}'))]+
                  [BasicTrait()])
    prototypeContainer.addPrototype(
        name        = 'Combat Hexes prototype',
        description = 'Check hexes for combat',
        traits      = traits)

    # Reset defender hexes to the empty string 
    resetHex = [
        GlobalPropertyTrait(
            ['',updateHex+f'Reset{no}',GlobalPropertyTrait.DIRECT, '{""}'],
            name        = f'{defenderHex}{no}',
            numeric     = False,
            description = 'Reset defender hex')
        for no in hexesRange]
    resetHex += [
        GlobalPropertyTrait(
            ['',updateHex+f'Reset{no}',GlobalPropertyTrait.DIRECT, '{-1}'],
            name        = f'{defenderHex}{no}X',
            numeric     = False,
            description = 'Reset defender hex X pixel')
        for no in hexesRange]
    resetHex += [
        GlobalPropertyTrait(
            ['',updateHex+f'Reset{no}',GlobalPropertyTrait.DIRECT, '{-1}'],
            name        = f'{defenderHex}{no}Y',
            numeric     = False,
            description = 'Reset defender hex Y pixel')
        for no in hexesRange]
    # Reset defender behind river hexes to "-"
    resetHex += [
        GlobalPropertyTrait(
            ['',updateHex+f'Reset{no}',GlobalPropertyTrait.DIRECT, '{"-"}'],
            name        = f'{defenderRiver}{no}',
            numeric     = False,
            description = 'Reset defender river hex')
        for no in hexesRange]
    # Reset defender behind ridge hexes to "-"
    resetHex += [
        GlobalPropertyTrait(
            ['',updateHex+f'Reset{no}',GlobalPropertyTrait.DIRECT, '{"-"}'],
            name        = f'{defenderRidge}{no}',
            numeric     = False,
            description = 'Reset defender ridge hex')
        for no in hexesRange]
    resetHex += [
        GlobalPropertyTrait(
            ['',updateHex+'Reset1',GlobalPropertyTrait.DIRECT, f'{0}'],
            name        = defenderJungle,
            numeric     = True,
            description = 'Some defender in Jungle')]
        
    # Reset keys 
    resetHexes = [updateHex+f'Reset{no}' for no in hexesRange]
        
    # ----------------------------------------------------------------
    # Battle calculations
    battleUnitP = prototypes[battleUnit]
    traits      = battleUnitP.getTraits()
    basic       = traits.pop()
    effdf       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name', value='EffectiveDF')
    effdf['expression'] = (f'{{DF*'
                           f'(BehindRiver?2:1)*'
                           f'(BehindRidge?2:1)'
                           #f'1'
                           f'}}')
    oddsh       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name', value='OddsShift')
    repdf       = None
    for trait in traits:
        if trait.ID != ReportTrait.ID:
            continue
        if 'EffectiveDF' not in trait['report']:
            continue
        repdf = trait
        break
    repdf['report'] = (f'{{{verbose}?("! "+BasicName+" @ "+'
                       f'LocationName.toString()+" add "+EffectiveDF+'
                       f'" to total defence factor ("+DF+'
                       f'",jungle="+InJungle+" (DRM "+{defenderJungle}+")"+'
                       f'",river="+BehindRiver+'
                       f'",ridge="+BehindRidge+' 
                       f'")"):""}}')
    traits.extend([
        PrototypeTrait(name='Combat Hexes prototype'),
        CalculatedTrait(
            name       = 'InJungle',
            expression = f'{{{jungles}.contains(":"+{ln}+":")}}'),
        ReportTrait(
            key('X'),
            report = f'{{{debug}?("~"+BasicName+" declare combat"):""}}'),
        RestrictCommandsTrait(
            name          = 'Restrict print to only when verbose',
            hideOrDisable = RestrictCommandsTrait.HIDE,
            expression    = f'{{!{debug}}}',
            keys          = [printKey]),
        TriggerTrait(
            name       = 'Print',
            command    = 'Print',
            key        = printKey,
            property   = '',#f'{{{debug}}}',
            actionKeys = []),
        ReportTrait(
            printKey,
            report=(f'{{BasicName+" @ "'
                    f'+{ln}+" ("'
                    f'+" Jungle="+InJungle'
                    f'+" AF="+EffectiveAF+" ("+CF+")"'
                    f'+" DF="+EffectiveDF+" ("+DF+")"'
                    f'+" Range="+Range'
                    f'+" Support="+Support'
                    f'+" Shift="+OddsShift'
                    f'+" Faction="+Faction'
                    f'+" Attacker="+IsAttacker'
                    f'+")"}}')),
        basic
    ])
    traits.insert(0,
                  RestrictCommandsTrait(
                      name          = 'Restrict Ctrl-X to movement and combat',
                      hideOrDisable = RestrictCommandsTrait.DISABLE,
                      expression    = f'{{{notDeclare}}}',
                      keys          = [key('X')]))

    battleUnitP.setTraits(*traits)

    # ----------------------------------------------------------------
    battleCalcP = prototypes[battleCalc]
    traits      = battleCalcP.getTraits()
    basic       = traits.pop()
    calcOdds    = Trait.findTrait(traits,TriggerTrait.ID,
                                  key   = 'key',         
                                  value = calcOddsKey)
    calcFrac    = Trait.findTrait(traits,TriggerTrait.ID,
                                  key   = 'key',         
                                  value = calcFracKey)
    batFrac    = Trait.findTrait(traits,CalculatedTrait.ID,
                                 key   = 'name',         
                                 value = 'BattleFraction')
    oddsIdx    = Trait.findTrait(traits,CalculatedTrait.ID,
                                 key   = 'name',         
                                 value = 'OddsIndex')
    oddsIdxL   = Trait.findTrait(traits,CalculatedTrait.ID,
                                 key   = 'name',         
                                 value = 'OddsIndexLimited')
    for t in traits:
        if t.ID != ReportTrait.ID:
            continue
        if '! Battle #' not in t['report']:
            continue

        rep = t['report']
        rep = rep.replace(battleOdds,
                          (f'{battleOdds}+" "+'
                           f'(DRM>0?"+":"")+DRM+" DRM"+'
                           f'"("+{battleShift}+"(ground support)+"+'
                           f'{defenderJungle}+"(jungle)+"+'
                           f'OverOdds+"(odds))"'
                           ))
        t['report'] = rep
        
    oldFrac               = batFrac['expression'][1:-1]
    oldIx                 = oddsIdx['expression'][1:-1]
    oddsIdx['expression'] = f'{{{oldIx.split("+")[0]}}}'

    traits.extend(resetHex)
    traits.extend([
        CalculatedTrait(
            name='OverOdds',
            expression = f'{{{battleFrac}>=4?1:0}}'),
        CalculatedTrait(
            name = 'DRM',
            expression = f'{{{battleShift}+{defenderJungle}+OverOdds}}'),
        # --- Update hex of defenders --------------------------------
        GlobalHotkeyTrait(
            name         = '',
            key          = updateHex+'Real',
            globalHotkey = updateHex,
            description  = 'Ask GKC to update defender hex'),
        # --- Update defender hexes -----------------------------------
        TriggerTrait(
            name       = '',
            key        = updateHex,
            actionKeys = resetHexes+[updateHex+'Real']),
        GlobalPropertyTrait(
            ['',publishDRM,GlobalPropertyTrait.DIRECT,'{DRM}'],
            name = combatDRM,
            numeric = True,
            description = 'Combat Die-roll modifier'),
        basic])
    keys = calcOdds.getActionKeys()
    keys.append(publishDRM)
    calcOdds.setActionKeys([updateHex]+keys)
    battleCalcP.setTraits(*traits)
        
         
    # ----------------------------------------------------------------
    # Battle calculations - I4  H2
    battleOddsP = prototypes['OddsMarkers prototype']
    traits      = battleOddsP.getTraits()
    basic       = traits.pop()
    battleres   = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name', value='BattleResult')
    die         = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='Die')
    getDet      = Trait.findTrait(traits,TriggerTrait.ID,
                                  key='key',
                                  value=getBattle+'Details')
    keys = getDet.getActionKeys()
    keys.append(getBattle+'DRM')
    getDet.setActionKeys(keys)
    print(keys)

    battleres['expression'] = '{OddsResult}'
    die['expression']       = (f'{{Math.max(1,Math.min(6,'
                               f'GetProperty("1d6Dice_result")'
                               f'+DRM))}}')

    traits.extend([
        DynamicPropertyTrait(
            ['',getBattle+'DRM',DynamicPropertyTrait.DIRECT,
             f'{{{combatDRM}}}'],
            name = 'DRM',
            numeric = True,
            value   = 0,
            description = 'Store DRM'),
        LabelTrait(
            label = (f'{{(DRM==0?"":"DRM "+(DRM>0?"+":"")+DRM)}}'),
            background = '255,255,255',
            foreground = '0,0,0',
            fontSize   = 16,
            menuCommand = '',
            vertical    = LabelTraitCodes.CENTER,
            verticalJust = LabelTraitCodes.BOTTOM,
            fontStyle    = LabelTraitCodes.BOLD
        ),
        ReportTrait(
            getBattle+'DRM',
            report = f'{{{debug}?("~ Took DRM="+{combatDRM}+" -> "+DRM):""}}'),
        ReportTrait(
            rollDie,
            report = (f'{{{verbose}?("! Rolled die "+'
                      f'GetProperty("1d6Dice_result")+'
                      f'(DRM>=0?"+":"")+'
                      f'DRM+"(DRM) -> "+Die):""}}'))])
    traits.extend([
        MarkTrait(name = pieceLayer, value = oddLayer),
        ])
    # Before the command to restrict
    traits.insert(0,
                  RestrictCommandsTrait(
                      name          = 'Restrict Ctrl-Y to combat',
                      hideOrDisable = RestrictCommandsTrait.DISABLE,
                      expression    = f'{{{notResolve}}}',
                      keys          = [key('Y')]))

    battleOddsP.setTraits(*traits,basic)

    # Modify the individual odds-markers to calculate battle result
    pieces = game.getPieces(asdict=True)
    for pn, piece in pieces.items():
        if not pn.startswith('odds marker'):
            continue

        odds   = pn.replace('odds marker','').strip()
        if odds == '1:1000':
            ijcalc = 'AE'
            uscalc = 'AE'
        else:
            ijrow  = ijCrt[odds]
            usrow  = usCrt[odds]
            ijcalc = ':'.join([f'(Die=={n+1})?"{r}"'
                               for n,r in enumerate(ijrow)])+':""'
            uscalc = ':'.join([f'(Die=={n+1})?"{r}"'
                               for n,r in enumerate(usrow)])+':""'
        # print(f'{odds}')
        # print(f'  {ijcalc}')
        # print(f'  {uscalc}')
        
        traits = piece.getTraits()
        basic  = traits.pop()
        traits.extend([
            CalculatedTrait(
                name='OddsResult',
                expression = f'{{Phase.contains("IJ")?({ijcalc}):({uscalc})}}'),
            basic])
        piece.setTraits(*traits)
        
        # ----------------------------------------------------------------
    # Modify odds prototype
    btlP   = prototypes['BattleMarkers prototype']
    traits = btlP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = btlLayer))
    btlP.setTraits(*traits,basic)

    # ----------------------------------------------------------------
    # Modify odds prototype
    resP   = prototypes['ResultMarkers prototype']
    traits = resP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = resLayer))
    resP.setTraits(*traits,basic)
    
    
    # ----------------------------------------------------------------
    # Fix faction prototypes
    for faction in ['IJ', 'US']:
        proto  = prototypes[f'{faction} prototype']
        traits = proto.getTraits()
        basic  = traits.pop()
        delt   = Trait.findTrait(traits,DeleteTrait.ID)
        if delt:
            traits.remove(delt)

        traits.extend([
            TriggerTrait(
                name       = '',
                key        = reinforceKey,
                actionKeys = [reinforceKey+'Real'],
                property   = (f'{{{autoReinforce}&&'
                              f'((Phase.contains("{faction} Reinforcements")'
                              f'&&Turn==ReinforceTurn)||'
                              f'(Phase.contains("Setup")&&Turn==1'
                              f'&&ReinforceTurn==0))}}')
            ),
            SendtoTrait(
                mapName     = 'Board',
                boardName   = 'Board',
                name        = '',
                zone        = f'{{ReinforceZoneReal}}',
                destination = SendtoTrait.ZONE,
                key         = reinforceKey+'Real'),
            ReportTrait(
                reinforceKey+'Real',
                report = (f'{{{verbose}?("! "+BasicName+'
                          f'" reinforce at "+ReinforceZoneReal):""}}')),
            CalculatedTrait(# Can be overriden by specific piece
                name        = 'ReinforceZoneReal',
                expression  = f'{{ReinforceZone!=""?ReinforceZone:"{faction} upmarch"}}'),
            MarkTrait(
                name       = pieceLayer,
                value      = unitLayer)])
        
        proto.setTraits(*traits,basic)
        
    # ----------------------------------------------------------------
    # Add some prototypes
    for pn,k in [
            ('USAF CAP',          'C'),
            ('USAF Ground Attack','B')]:
        ks     = key(k,CTRL_SHIFT)
        traits = [ReportTrait(g['key']),
                  SendtoTrait(mapName     = 'Board',
                              boardName   = 'Board',
                              name        = f'Send to {pn}',
                              restoreName = '',
                              restoreKey  = '',
                              zone        = f'{pn} pool',
                              destination = 'Z', #R for region
                              key         = ks,
                              x           = 0,
                              y           = 0),
                  BasicTrait()]
        prototypeContainer.addPrototype(
            name        = f'{pn} prototype',
            description = f'Send to {pn} prototype',
            traits      = traits)

    traits = [ReportTrait(key('E',CTRL_SHIFT)),
              SendtoTrait(mapName     = 'DeadMap',
                          boardName   = 'IJ pool',
                          name        = '',
                          key         = key('E',CTRL_SHIFT),
                          description = "Retire unit"),
              BasicTrait()]
    prototypeContainer.addPrototype(
        name        = f'IJ Retire prototype',
        description = f'Send to IJ Retire prototype',
        traits      = traits)
        

    # ----------------------------------------------------------------
    airstrip = []
    for an,az,ak in [('Henderson',['0709','0708'],['1','2']),
                     ('Fighter',['0605','0705'],['3','4'])]:
        for z,k in zip(az,ak):
            pn     = f'{an} {z}'
            keys   = key(k,CTRL_SHIFT)
            traits = [ReportTrait(keys),
                      SendtoTrait(mapName     = 'Board',
                                  boardName   = 'Board',
                                  name        = f'Send to {pn}',
                                  restoreName = '',
                                  restoreKey  = '',
                                  zone        = f'{pn} pool',
                                  region      = f'{an} {z}',
                                  destination = 'R', #R for region
                                  key         = keys,
                                  x           = 0,
                                  y           = 0),
                      BasicTrait()]
            prototypeContainer.addPrototype(
                name        = f'{pn} prototype',
                description = f'Send to {pn} prototype',
                traits      = traits)
            airstrip.append(pn)

    # ----------------------------------------------------------------
    traits = [
        TriggerTrait(
            name       = '',
            command    = '',
            key        = checkSupply,
            actionKeys = [checkSupply+'Die',
                          checkSupply+'End']),
        TriggerTrait(
            name       = '',
            command    = '',
            key        = checkSupply+'End',
            actionKeys = []),
        GlobalHotkeyTrait(
            name         = '',
            key          = checkSupply+'Die',
            globalHotkey = diceKey,
            description  = 'Roll dice'),        
        CalculatedTrait(
            name = 'Die',
            expression = '{GetProperty("1d6Dice_result")}'
        ),
        ReportTrait(
            checkSupply+'End',
            report = ('{(Die>3?"~":"!")+"IJ supply roll: "+Die+" "+'
                      '(Die>3?"_must_ remove one ground unit or destroyer"+'
                      '" from IJN Reserve":" no losses")}')),
        BasicTrait()]
    prototypeContainer.addPrototype(name='Check supply prototype',
                                    traits=traits)

    turns.addHotkey(hotkey       = checkSupply,
                    match        = (f'{{{autoSupply}&&Phase=="IJ Supply"}}'),
                    reportFormat = '',
                    name         = 'Check IJ supply')
    main.addMassKey(name         = 'Check supply',
                    buttonText   = '',
                    buttonHotkey = checkSupply,
                    hotkey       = checkSupply,
                    icon         = 'supply-icon.png', #'airstrip-icon.png',
                    tooltip      = 'Roll for IJ supply', #Check USAF capacity',
                    target       = '',
                    filter       = ('{BasicName == "game turn"}'),
                    canDisable   = True,
                    propertyGate = notSupply,
                    reportFormat = '',
                    reportSingle = True)
    
      
    # ----------------------------------------------------------------
    # Get all pieces - and litterally all.  That means we cannot get
    # them in a dict, since that will filter out on unique keys.
    pieces = game.getAllElements(PieceSlot,single=False)
    
    # ----------------------------------------------------------------
    # First pass to change airfield marker
    oper = None
    dests = []
    opers = []
    for p in pieces:
        pn     = p.getAttribute('entryName')
        if not pn.endswith('airfield'):
            continue

        if 'destroyed' in pn:
            dests.append(p)
            
        elif 'operational' in pn:
            if not oper:
                oper = p
            else:
                opers.append(p)

    traits = oper.getTraits()
    basic  = traits.pop()

    rot    = Trait.findTrait(traits,RotateTrait.ID)
    if rot: traits.remove(rot)
    rep    = Trait.findTrait(traits,ReportTrait.ID)
    traits.remove(rep)
    
    layer  = Trait.findTrait(traits,LayerTrait.ID,
                             key='name',value='Step')
    imgs = layer['images'].split(',')
    imgs.append('destroyed_airfield.png')
    
    layer['images']   = ','.join(imgs)
    layer['newNames'] = ','.join(['Operational airfield',
                                  'Damaged airfield',
                                  'Destroyed airfield'])
    layer['name'] = 'Airfield'
    layer['loop'] = False
    layer['activateName'] = ''
    layer['increaseName'] = 'Worsen'
    layer['decreaseName'] = 'Improve'
    layer['increaseKey']  = key('F',CTRL_SHIFT)
    layer['decreaseKey']  = key('F')

    rep = ReportTrait(
        key('F'),key('F',CTRL_SHIFT),
        report = ('{LocationName.toString()+" is now "+'
                  'PieceName+" ("+Airfield_Level+")"}'))
    traits.append(rep)
    traits.append(MarkTrait(name='Airfield',value='true'))
    traits.append(
        TriggerTrait(
            name       = '',
            key        = reinforceKey,
            actionKeys = [key('1',CTRL_SHIFT)],
            property   = (f'{{{autoReinforce}&&Turn==1&&Phase.contains("Setup")}}')))
        
    traits.extend([PrototypeTrait(f'{pn} prototype') for pn in airstrip])

    oper.setTraits(*traits,basic)

    reinfKeys = list(range(1,5))[::-1]
    for o in opers:
        parent = o.getParent()
        parent.remove(o)
        
        if not isinstance(parent,AtStart):
            continue
        
        oo     = parent.addPiece(oper)
        traits = oo.getTraits()
        for trait in traits:
            if isinstance(trait,TriggerTrait):
                trait.setActionKeys([key(str(reinfKeys.pop()),CTRL_SHIFT)])
                print(trait['actionKeys'])
        
        oo.setTraits(*traits)

    for d in dests:
        parent = d.getParent()
        print(parent)
        if parent: parent.remove(d)

        if not isinstance(parent,AtStart):
            continue
        
        dd     = parent.addPiece(oper)
        traits = dd.getTraits()
        for trait in traits:
            if isinstance(trait,LayerTrait):
                trait['level'] = 3
            if isinstance(trait,TriggerTrait):
                trait.setActionKeys([key(str(reinfKeys.pop()),CTRL_SHIFT)])
                print(trait['actionKeys'])
                    
        dd.setTraits(*traits)

    traits = [
        TriggerTrait(
            name       = '',
            command    = '',
            key        = countAirstrips,
            actionKeys = [countAirstrips+'Count',
                          countAirstrips+'USAF',
                          countAirstrips+'End']),
        TriggerTrait(
            name       = '',
            command    = '',
            key        = countAirstrips+'End',
            actionKeys = []),
        DynamicPropertyTrait(
            ['',countAirstrips+'Count',DynamicPropertyTrait.DIRECT,
             '{CountMap("{Airfield_Level==1}")}'],
            name = 'AirfieldCount',
            numeric = True,
            value   = 0),
        DynamicPropertyTrait(
            ['',countAirstrips+'USAF',DynamicPropertyTrait.DIRECT,
             '{CountMap("{IsUSAF==1&&NotUSAFReserver==1}")}'],
            name = 'USAFCount',
            numeric = True,
            value   = 0),
        CalculatedTrait(
            name = 'USAFToEliminate',
            expression = '{Math.max(USAFCount-6*AirfieldCount,0)}'),
        # ReportTrait(
        #     countAirstrips,
        #     report = '{"Counting airstrips and USAF"}'),
        # ReportTrait(
        #     countAirstrips+'Count',
        #     report = '{"Airfield count is "+AirfieldCount}'),
        # ReportTrait(
        #     countAirstrips+'USAF',
        #     report = '{"USAF count is "+USAFCount}'),
        ReportTrait(
            countAirstrips+'End',
            report = ('{(USAFToEliminate>0?"~":"!")+'
                      '" Functional airstrips: "+AirfieldCount+'
                      '", "+(AirfieldCount*6)+" USAF units supported, "+'
                      'USAFCount+" currently active"+'
                      '(USAFToEliminate>0?(", "+USAFToEliminate+'
                      '" _must_ be put in reserve"):"")}')),
        BasicTrait()]
    prototypeContainer.addPrototype(name='Airfield count prototype',
                                    traits=traits)

    turns.addHotkey(hotkey       = countAirstrips,
                    match        = ('{Phase=="US Air"}'),
                    reportFormat = '',
                    name         = 'Check USAF capacity')
    main.addMassKey(name         = 'Count airfields',
                    buttonText   = '',
                    buttonHotkey = countAirstrips,
                    hotkey       = countAirstrips,
                    icon         = 'airstrip-icon.png',
                    tooltip      = 'Check USAF capacity',
                    target       = '',
                    filter       = ('{BasicName == "game turn"}'),
                    canDisable   = True,
                    propertyGate = notAir,
                    reportFormat = '',
                    reportSingle = True)

    # ----------------------------------------------------------------
    # Process all pieces 
    pieces = game.getAllElements(PieceSlot,single=False)
    for p in pieces:
        protos = None
        pn     = p.getAttribute('entryName')
        nt     = []
        ground = False
        traits = p.getTraits()
        basic  = traits.pop()
        rzone  = None
        turn   = turnNo(traits)
        print(f'{turn:3d} {pn}')
        # Check if we need to add prototypes to the piece 
        if pn.startswith('ijn'):
            protos = ['IJN Attack', 'IJ Retire']
            ground = True
        elif pn.startswith('ijaaf'):
            protos = ['IJAAF Attack', 'IJ Retire']
            nt.append(
                MarkTrait(name='ReinforceZone',
                          value='IJAAF Reserve pool'))
        elif pn.startswith('usaf'):
            protos = ['USAF Reserve']
            if any([f in pn for f in ['usaf f4f','usaf p400','usaf p38']]):
                protos.append('USAF CAP')
            if any([f in pn for f in ['usaf f4f','usaf sdb','usaf tbf']]):
                protos.append('USAF Ground Attack')
                ground = True
            protos += airstrip
            nt.extend([
                MarkTrait(name='IsUSAF',value=1),
                CalculatedTrait(
                    name='NotUSAFReserver',
                    expression = '{LocationName!="USAF Reserve pool"?1:0}')
            ])
            if turn > 0:
                nt.append(
                    MarkTrait(name='ReinforceZone',
                              value='USAF Reserve pool'))
                
        elif pn.startswith('construction'):
            protos = airstrip
            nt.append(MarkTrait(name=pieceLayer,value=airLayer))
        elif pn.endswith('airfield'):
            protos = airstrip
            nt.append(MarkTrait(name=pieceLayer,value=airLayer))
        elif pn == "game turn":
            protos = turnp+['Check combat','Check supply','Airfield count']
            nt = [RestrictAccessTrait(sides=[], description='Cannot move')]
        #elif pn == hidden:
        #    protos =
        if Trait.findTrait(traits,MarkTrait.ID,key='name', value='Range'):
            if not protos: protos = []
            protos += ['Artillery range']

        if turn >= 0:
            traits.append(MarkTrait(
                name = 'ReinforceTurn',
                value = turn))
            
        if ground:
            try:
                factors  =  Trait.findTrait(traits,MarkTrait.ID,
                                            key='name', value='factors')
                if not factors:
                    val = 0
                    if pn == 'ijn dd':
                        val = '+2 B';
                    if pn == 'usaf f4f':
                        val = '+1 F'
                    if pn == 'usaf sdb' or pn == 'usaf tbf':
                        val = '+1'

                    factors = {'value': f'chit 1 factor={val}'}
                    # raise RuntimeError(f'Piece {pn} is marked as '
                    #                    f'ground support unit, but has no '
                    #                    f'factors to look at!')
                value    =  factors['value']
                stripd   =  value.replace('chit 1 factor=','').strip('FB ')
                odds     =  int(stripd)
                # protos   += ['Ground support']
                # Before (in list, below in editor) of faction prototype
                # so that it is seen before the battle unit prototype and
                # can override traits there. 
                traits.insert(0,
                              PrototypeTrait(name="Ground support prototype"))
                traits.append(MarkTrait(name='Support',value=odds))
                #traits.append(MarkTrait(name='OddsShift',value=odds))
            except Exception as e:
                from sys import stderr
                print(e,file=stderr)
                pass 
            
        if protos is not None:
            traits.extend([PrototypeTrait(ppn + ' prototype')
                           for ppn in protos])
        if nt is not None:
            traits.extend(nt)


        pn     = basic['name']
        pretty = pretty_name(pn)
        basic['name'] = pretty
            
        traits.append(basic)
        p.setTraits(*traits)


#
# EOF
#

                      
    
    
